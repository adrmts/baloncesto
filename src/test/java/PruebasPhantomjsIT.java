import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.constraints.Size;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class PruebasPhantomjsIT
{
private static WebDriver driver=null;
@Test
    public void tituloIndexTest()
    {
    DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println("Titulo obtenido: "+driver.getTitle());
        driver.close();
        driver.quit();
    }
    @Test
    public void VotosACeroTest()
    {
        List<WebElement> votosList;
        int votosSum = 1;
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.name("B3")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.name("B4")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        votosList = driver.findElements(By.xpath("//td[contains(@name,'V')]"));
        System.out.println("numero de jugadores :" +votosList.size());
        for (WebElement voto: votosList){
            votosSum = votosSum * Integer.parseInt(voto.getText());
        }
        assertEquals(0, votosSum, "Los votos no se han puesto a 0");
        System.out.println("Suma de votos obtenidos "+votosSum);
        driver.close();
        driver.quit();
    }

    @Test
    public void VotarOtroTest()
    {
        List<WebElement> votosList;
        String jugador = "Navarro";
        int votos =-1;
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        driver.findElement(By.name("txtOtros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys(jugador);
        driver.findElement(By.xpath("//input[contains(@value,'Otros')]")).click(); //Hay q localizar el R1 correcto
        driver.findElement(By.name("B1")).click();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.name("index")).click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.name("B4")).click();

        votosList = driver.findElements(By.xpath("//td[contains(@name,'V')]"));
        System.out.println("numero de jugadores :" +votosList.size());
        for (int i =0; i<votosList.size();i++){
            
            if(driver.findElement(By.name("R"+(i+1))).getText().equals(jugador)){
                votos = Integer.parseInt(votosList.get(i).getText());
            }
        }
        assertEquals(1, votos, "No se ha producido el voto a "+jugador);
        System.out.println("Suma de votos obtenidos "+votos);
        driver.close();
        driver.quit();
    }
}