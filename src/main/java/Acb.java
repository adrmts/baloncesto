import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
        //comment
    }
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
        String votos = (String) req.getParameter("votos");
        String ver = (String) req.getParameter("ver");
        
        if (votos!=null){
            bd.ponerVotosACero();
            res.sendRedirect(res.encodeRedirectURL("index.html"));
            return;
        }
        if (ver!=null){
   
            s.setAttribute("tabla_jugadores",bd.buscarTodosJugadores() );
            res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
            return;
        }
        if(nombre!=null){
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
                System.out.println("Pasa por aqui");
            } else {
                bd.insertarJugador(nombre);
            }
        }
        s.setAttribute("nombreCliente", nombreP);
        // Llamada a la página jsp que nos da las gracias
        res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
    }
    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();

    }
}
