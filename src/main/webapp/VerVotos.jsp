<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Votaci&oacute;n mejor jugador liga ACB. Tabla votos</title>
        <link href="estilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="resultado">
        <H1 class="titulo">Votaci&oacute;n al mejor jugador de la liga ACB</H1>
        <hr>
            <table class="tabla">
                <thead>
                <tr>
                    <td>Jugador</td>
                    <td>votos</td>
                </tr>
                </thead>
                <tbody>
                <%
                    String tabla_jugadores = (String) session.getAttribute("tabla_jugadores");
                    String tabla[] = tabla_jugadores.split(";");
                    int limite = (tabla.length-1);
                    for(int i=0; i<limite ;i=i+2){
                        out.print(String.format("<tr><td name=R%s>%s</td>" +
                                        "<td name=V%s>%s</td></tr>",(i/2)+1,tabla[i],(i/2)+1,tabla[i+1]));
                    }
                %>
                </tbody>
            </table>
         </hr>
       <a href="index.html"> Ir al comienzo</a>
    </body>
</html>
